package ru.ermolaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    void createTask(@Nullable String userId, @Nullable String name) throws Exception;

    void createTask(@Nullable String userId, @Nullable String name, String description) throws Exception;

    void addTask(@Nullable String userId, @Nullable Task task) throws Exception;

    @NotNull
    List<Task> findAllTasks(@Nullable String userId) throws Exception;

    void removeTask(@Nullable String userId, @Nullable Task task) throws Exception;

    void removeAllTasks(@Nullable String userId) throws Exception;

    @NotNull
    Task findTaskById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull
    Task findTaskByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    @NotNull
    Task findTaskByName(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    Task updateTaskById(@Nullable String userId, @Nullable String id, @Nullable String name, String description) throws Exception;

    @NotNull
    Task updateTaskByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, String description) throws Exception;

    @NotNull
    Task removeTaskById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull
    Task removeTaskByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    @NotNull
    Task removeTaskByName(@Nullable String userId, @Nullable String name) throws Exception;

}
