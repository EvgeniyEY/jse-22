package ru.ermolaev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.User;
import ru.ermolaev.tm.enumeration.Role;

public interface IUserService extends IService<User> {

    @Nullable
    User create(@Nullable String login, @Nullable String password) throws Exception;

    @Nullable
    User create(@Nullable String login, @Nullable String password, @Nullable String email) throws Exception;

    @Nullable
    User create(@Nullable String login, @Nullable String password, @Nullable Role role) throws Exception;

    @Nullable
    User updatePassword(@Nullable String userId, @Nullable String newPassword) throws Exception;

    @Nullable
    User findById(@Nullable String id) throws Exception;

    @Nullable
    User findByLogin(@Nullable String login) throws Exception;

    @Nullable
    User findByEmail(@Nullable String email) throws Exception;

    @Nullable
    User removeUser(@Nullable User user);

    @Nullable
    User removeById(@Nullable String id) throws Exception;

    @Nullable
    User removeByLogin(@Nullable String login) throws Exception;

    @Nullable
    User removeByEmail(@Nullable String email) throws Exception;

    @Nullable
    User updateUserFirstName(@Nullable String userId, @Nullable String newFirstName) throws Exception;

    @Nullable
    User updateUserMiddleName(@Nullable String userId, @Nullable String newMiddleName) throws Exception;

    @Nullable
    User updateUserLastName(@Nullable String userId, @Nullable String newLastName) throws Exception;

    @Nullable
    User updateUserEmail(@Nullable String userId, @Nullable String newEmail) throws Exception;

    @Nullable
    User lockUserByLogin(@Nullable String login) throws Exception;

    @Nullable
    User unlockUserByLogin(@Nullable String login) throws Exception;

}
