package ru.ermolaev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.Session;
import ru.ermolaev.tm.entity.Task;

import java.util.List;

public interface ITaskEndpoint {

    void createTask(@Nullable Session session, @Nullable String name, @Nullable String description) throws Exception;

    void clearTasks(@Nullable Session session) throws Exception;

    @NotNull
    List<Task> showAllTasks(@Nullable Session session) throws Exception;

    @NotNull
    Task showTaskById(@Nullable Session session, @Nullable String id) throws Exception;

    @NotNull
    Task showTaskByName(@Nullable Session session, @Nullable String name) throws Exception;

    @NotNull
    Task updateTaskById(@Nullable Session session, @Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    @NotNull
    Task removeTaskById(@Nullable Session session, @Nullable String id) throws Exception;

    @NotNull
    Task removeTaskByName(@Nullable Session session, @Nullable String name) throws Exception;

}
