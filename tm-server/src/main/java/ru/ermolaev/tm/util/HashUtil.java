package ru.ermolaev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface HashUtil {

    @NotNull
    String KEY = "159753";

    @NotNull
    Integer ITERATION = 547;

    @Nullable
    static String hidePassword(@NotNull final String value) {
        @Nullable String result = value;
        for (int i = 0; i < ITERATION; i++) {
            result = md5(KEY + result + KEY);
        }
        return result;
    }

    static @Nullable String md5(@NotNull final String value) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            @NotNull final byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}
