package ru.ermolaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.api.repository.IRepository;
import ru.ermolaev.tm.api.service.IService;
import ru.ermolaev.tm.entity.AbstractEntity;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    private final IRepository<E> repository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public void load(@Nullable final List<E> es) {
        if (es == null) return;
        repository.load(es);
    }

    @Override
    public void load(@Nullable final E ... es) {
        if (es == null) return;
        repository.load(es);
    }

}
