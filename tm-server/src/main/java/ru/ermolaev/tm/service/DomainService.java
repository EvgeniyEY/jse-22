package ru.ermolaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.api.service.IDomainService;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.api.service.ITaskService;
import ru.ermolaev.tm.api.service.IUserService;
import ru.ermolaev.tm.dto.Domain;

public final class DomainService implements IDomainService {

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final IUserService userService;

    public DomainService(
            @NotNull final ITaskService taskService,
            @NotNull final IProjectService projectService,
            @NotNull final IUserService userService
    ) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.userService = userService;
    }

    @Override
    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
        taskService.load(domain.getTasks());
        projectService.load(domain.getProjects());
        userService.load(domain.getUsers());
    }

    @Override
    public void export(@Nullable final Domain domain) {
        if (domain == null) return;
        domain.setTasks(taskService.findAll());
        domain.setProjects(projectService.findAll());
        domain.setUsers(userService.findAll());
    }

}
