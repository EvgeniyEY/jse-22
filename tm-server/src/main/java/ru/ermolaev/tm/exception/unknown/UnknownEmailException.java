package ru.ermolaev.tm.exception.unknown;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.exception.AbstractException;

public final class UnknownEmailException extends AbstractException {

    public UnknownEmailException() {
        super("Error! This email does not exist.");
    }

    public UnknownEmailException(@NotNull final String email) {
        super("Error! This email [" + email + "] does not exist.");
    }

}
