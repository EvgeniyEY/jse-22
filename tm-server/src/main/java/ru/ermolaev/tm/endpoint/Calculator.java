package ru.ermolaev.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;

@WebService
public final class Calculator {

    @WebMethod
    public int sum(
            @WebParam(name = "a") final int a,
            @WebParam(name = "b") final int b
    ) {
        return a + b;
    }

    public static void main(String[] args) {
        final Calculator calculator = new Calculator();
        Endpoint.publish("http://127.0.0.1:9090/Calculator?wsdl", calculator);
    }

}
