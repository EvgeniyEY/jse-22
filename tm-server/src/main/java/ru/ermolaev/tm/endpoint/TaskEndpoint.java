package ru.ermolaev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.api.endpoint.ITaskEndpoint;
import ru.ermolaev.tm.api.service.ServiceLocator;
import ru.ermolaev.tm.entity.Session;
import ru.ermolaev.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public final class TaskEndpoint implements ITaskEndpoint {

    private ServiceLocator serviceLocator;

    public TaskEndpoint() {
    }

    public TaskEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public void createTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "id") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().createTask(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public void clearTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeAllTasks(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> showAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAllTasks(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public Task showTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findTaskById(session.getUserId(), id);
    }

    @NotNull
    @Override
    @WebMethod
    public Task showTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findTaskByName(session.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public Task updateTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateTaskById(session.getUserId(), id, name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public Task removeTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeTaskById(session.getUserId(), id);
    }

    @NotNull
    @Override
    @WebMethod
    public Task removeTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeTaskByName(session.getUserId(), name);
    }

}
