package ru.ermolaev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.api.endpoint.ISessionEndpoint;
import ru.ermolaev.tm.api.service.ServiceLocator;
import ru.ermolaev.tm.dto.Fail;
import ru.ermolaev.tm.dto.Result;
import ru.ermolaev.tm.dto.Success;
import ru.ermolaev.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class SessionEndpoint implements ISessionEndpoint {

    private ServiceLocator serviceLocator;

    public SessionEndpoint() {
    }

    public SessionEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    @WebMethod
    public Session openSession(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) throws Exception {
        return serviceLocator.getSessionService().open(login, password);
    }

    @NotNull
    @Override
    @WebMethod
    public Result closeSession(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getSessionService().close(session);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result closeAllSessionsForUser(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getSessionService().closeAll(session);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

}
