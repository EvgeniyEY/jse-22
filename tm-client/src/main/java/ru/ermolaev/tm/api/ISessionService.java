package ru.ermolaev.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.endpoint.Session;

public interface ISessionService {

    @Nullable
    Session getCurrentSession();

    void setCurrentSession(Session currentSession);

    void clearCurrentSession();

}
