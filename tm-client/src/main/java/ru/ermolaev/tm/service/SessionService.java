package ru.ermolaev.tm.service;

import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.endpoint.Session;

public final class SessionService implements ISessionService {

    @Nullable
    private Session currentSession;

    @Nullable
    @Override
    public Session getCurrentSession() {
        return currentSession;
    }

    @Override
    public void setCurrentSession(Session currentSession) {
        this.currentSession = currentSession;
    }

    @Override
    public void clearCurrentSession() {
        currentSession = null;
    }

}
