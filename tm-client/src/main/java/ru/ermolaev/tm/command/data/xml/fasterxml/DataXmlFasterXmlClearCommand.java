package ru.ermolaev.tm.command.data.xml.fasterxml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.Session;
import ru.ermolaev.tm.enumeration.Role;

public final class DataXmlFasterXmlClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "data-xml-fx-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove XML (fasterXML) file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE XML (FASTERXML) FILE]");
        @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getAdminDataEndpoint().clearXmlFileFasterXml(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
