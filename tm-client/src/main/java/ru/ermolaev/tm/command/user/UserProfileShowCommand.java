package ru.ermolaev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.Session;
import ru.ermolaev.tm.endpoint.User;
import ru.ermolaev.tm.enumeration.Role;

public final class UserProfileShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "user-show-profile";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show information about user account.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW USER PROFILE]");
        @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
        @Nullable final User user = serviceLocator.getUserEndpoint().showUserProfile(session);
        System.out.println("login: " + user.getLogin());
        System.out.println("e-mail: " + user.getEmail());
        System.out.println("first name: " + user.getFirstName());
        System.out.println("middle name: " + user.getMiddleName());
        System.out.println("last name: " + user.getLastName());
        System.out.println("user role: " + user.getRole());
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
