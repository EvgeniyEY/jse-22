package ru.ermolaev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.Session;
import ru.ermolaev.tm.enumeration.Role;

public final class ProjectsClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "project-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Delete all projects.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CLEAR PROJECTS]");
        @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getProjectEndpoint().clearProjects(session);
        System.out.println("[COMPLETE]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
