package ru.ermolaev.tm.command.data.json.jaxb;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.Session;
import ru.ermolaev.tm.enumeration.Role;

public final class DataJsonJaxbSaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "data-json-jb-save";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to json (Jax-B) file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON (JAX-B) SAVE]");
        @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getAdminDataEndpoint().saveJsonByJaxb(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
